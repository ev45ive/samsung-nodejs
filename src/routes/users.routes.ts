
import { RequestHandler } from 'express'
import fs from 'fs/promises'
// import fs from 'fs'
import multer from 'multer'
import { onlyLoggedIn, onlyAdmin } from '../config/middlewares'
import { createUser, CreateUserPayload, getAllUsers, getUserById, logInUser } from '../services/users.service'

// Promise with PROPER Error handling
import Router from 'express-promise-router'
import passport from 'passport'

export const usersRoutes = Router()

usersRoutes.get<{}>('/', async (req, res) => {
  const usersdata = await getAllUsers()

  res.json(usersdata)
})

// usersRoutes.post<{}, {}, { username: string, password: string }>('/login', async (req, res) => {
//   const { username, password } = req.body
//   const user = await logInUser(username, password)

//   res.json(user)
// })
usersRoutes.post('/login', passport.authenticate('local', {}), (req, res) => {
  if (req.user) {
    req.session.save()
    res.json(req.user)
  } else {
    res.json({ error: true })
  }
})


declare global {
  namespace Express {
    interface User { _id: string, name: string }
  }
}

usersRoutes.get('/me', (req, res) => {
  if (req.user) {
    res.json(req.user)
  }
})



usersRoutes.post<{}, {}, CreateUserPayload>('/', async (req, res) => {
  const payload = req.body;

  const usersdata = await createUser(payload)

  res.json(usersdata)
})


/**
 * This interface allows you to declare additional properties on your session object using [declaration merging](https://www.typescriptlang.org/docs/handbook/declaration-merging.html).
 *
 * @example
 * declare module 'express-session' {
 *     interface SessionData {
 *         views: number;
 *     }
 * }
 *
 */
declare module 'express-session' {
  interface SessionData {
    counter: number;
  }
}



usersRoutes.get('/session', (req, res) => {
  // req.isAdmin?.valueOf
  // req.sessionID
  req.session = req.session || { counter: 0 }
  req.session.counter = (req.session.counter || 0) + 1
  req.session.save()

  res.send({
    counter: req.session.counter
  })
})

/* Create User */
usersRoutes.post('/', onlyLoggedIn(['admin']), (req, res) => {
  req.body;
  debugger
  res.json({ user: { name: 'Me' } })
})

/* Update User */
usersRoutes.put('/:user_id',
  onlyLoggedIn(['admin']), (req, res) => {
    res.json({ user: { name: 'Me' } })
  })

usersRoutes.get<{ user_id: string }>('/:user_id', async (req, res) => {
  const { user_id } = req.params;
  const usersdata = await getUserById(user_id)

  res.json(usersdata)
})

const skipIfSmth: RequestHandler = (req, res, next) => {
  if (req.headers['x-some-special-header']) { next() }
  else { next('route') }
}

usersRoutes.get('/some', skipIfSmth, (req, res) => {
  res.json({ message: 'Special secret' })
})

usersRoutes.get('/some', (req, res) => {
  res.json({ message: 'Regular not secret' })
})


usersRoutes.get('/register', (req, res) => {
  res.json({ message: 'No registrations!' })
})

const upload = multer({
  dest: './public/uploads',
})

usersRoutes.post('/photo-upload', upload.single('photo'), /* upload.array('gallery'), */ async (req, res, next) => {
  // multipart/form-data; boundary=----WebKitFormBoundaryMct6MksVgujEC9wH
  // const output = fs.createWriteStream('./data/output.txt')
  const { id } = req.body
  // req.file.fieldname
  // req.file.filename
  // req.file.size
  // req.file.originalname
  // req.file.path
  // req.file;
  try {

    const dirPath = './public/photos/' + parseInt(id)
    await fs.mkdir(dirPath, { recursive: true })
    const filePath = dirPath + '/avatar.jpg'
    await fs.rename(req.file.path, filePath)
    res.send({ message: 'ok', filePath })

  } catch (error) {
    next(error)
  }


  // fs.mkdir('./public/photos/' + parseInt(id), { recursive: true }, () => () => {
  //   const filePath = './public/photos/' + id + '/avatar.jpg'
  //   fs.rename(req.file.path, filePath, () => {
  //     res.send({ message: 'ok', filePath })
  //   })
  // })

  // req.pipe(output)
  // req.pipe(res)
})