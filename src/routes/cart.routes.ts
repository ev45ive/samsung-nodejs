
import { Router } from 'express'
import path from 'path'
import fs from 'fs'
import { PageNotFoundError } from '../errors'

export const cartRoutes = Router()

declare module 'express-session' {
  interface SessionData {
    cart: Cart;
  }
}

interface SimpleProduct {
  id: string;
  name: string;
  price: number;
}

interface CartItem {
  id: string
  name: string
  price: number
  amount: number,
  product?: SimpleProduct
}

interface Cart {
  items: CartItem[];
  total: number;
}

// const carts: { [id: string]: Cart } = {
//   "session123": { items: [{ id: '1270', name: 'test', price: 100, amount: 100 }], total: 100 },
//   "sesion234": { items: [{ id: '1270', name: 'test', price: 100, amount: 100 }], total: 100 },
// }

// GET /cart
cartRoutes.get('/', (req, res) => {
  const cart = req.session.cart = req.session.cart || createEmptyCart()
  cart.total = cart.items.reduce((sum, item) => (item.price * item.amount) + sum, 0)

  res.json(cart)
})

// POST /cart/ {id:'123'}
cartRoutes.post('/', (req, res) => {
  const cart = req.session.cart = req.session.cart || createEmptyCart()
  const { id, amount = 1 } = req.body

  let item = cart.items.find(p => p.id == id)
  if (item) {
    item.amount += amount
  } else {
    item = { id, amount, name: '', price: 100 }
    cart.items.push(item)
  }
  cart.total = cart.items.reduce((sum, item) => (item.price * item.amount) + sum, 0)
  res.json(item)
})

cartRoutes.put('/:item_id', (req, res) => {
  const cart = req.session.cart = req.session.cart || createEmptyCart()
  const { amount = 1 } = req.body
  const { item_id } = req.params

  let item = cart.items.find(p => p.id == item_id)
  if (item) {
    item.amount += amount
  } else {
    throw new PageNotFoundError('Item is not in the cart')
  }
  cart.total = cart.items.reduce((sum, item) => (item.price * item.amount) + sum, 0)
  res.json(cart)
})


cartRoutes.delete('/:item_id', (req, res) => {
  const cart = req.session.cart = req.session.cart || createEmptyCart()
  const { amount = 1 } = req.body
  const { item_id } = req.params

  let index = cart.items.findIndex(p => p.id == item_id)
  if (index !== -1) {
    cart.items.splice(index, 1)
  }
  cart.total = cart.items.reduce((sum, item) => (item.price * item.amount) + sum, 0)
  res.json(cart)
})

function createEmptyCart(): Cart {
  return { items: [ ], total: 0 }
}

