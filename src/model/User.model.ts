import mongoose, { ObjectId, Document } from "mongoose";

const UserSchema = new mongoose.Schema({
  // _id: {
  //   type: mongoose.Types.ObjectId
  // },
  name: {
    type: String, index: true
  },
  password: {
    type: String,
    required: true,
    select: false
  },
  roles: [{ type: String }]
}, {
  autoCreate: true,
  autoIndex: true
})


export interface User extends Document {
  name: string,
  password?: string
}

// TIP: Fully Typed Model + Simple Data Interfaces
// https://medium.com/@agentwhs/complete-guide-for-typescript-for-mongoose-for-node-js-8cc0a7e470c1

export const User = mongoose.model<User>('user', UserSchema)