import { PageNotFoundError } from "../errors"
import { User } from "../model/User.model"



// type CreateUserPayload = Pick<User,'name'>
export type CreateUserPayload = Exclude<User, 'id'> & {
  password: string
}



export const getAllUsers = async (): Promise<User[]> => {
  return User.find()
}

export const getUserById = async (id: string) => {
  const result = await User.findById(id)

  if (!result) {
    throw new PageNotFoundError('User not found')
  }
  return result
}

import bcrypt from 'bcrypt'
import { AccessDenied } from "../errors/AccessDenied"

export const createUser = async (payload: CreateUserPayload) => {
  let { password, ...userData } = payload

  password = await bcrypt.hash(password, 10)

  const result = await User.create({
    ...userData,
    password
  })
  return result
}

export const logInUser = async (username: string, checkpassword: string) => {
  const user = await User.findOne({ name: username }, {
    name: 1,
    password: 1
  })
  if (!user) { throw new PageNotFoundError('User does not exist') }

  if (!(await bcrypt.compare(checkpassword, user.password!))) {
    throw new AccessDenied('Password do not match')
  }

  const { password, ...userData } = user.toJSON()
  return userData;
}