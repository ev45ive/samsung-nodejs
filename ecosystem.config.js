module.exports = {
  apps : [{
    name: "app",
    script: "./dist/index.js",
    exec_mode : "cluster",

    env: {
      NODE_ENV: "development",
    },
    env_production: {
      NODE_ENV: "production",
    }
  }]
}