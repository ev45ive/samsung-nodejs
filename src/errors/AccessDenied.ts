

export class AccessDenied extends Error {
  message = 'AccessDenied';
  status = 401;
}
