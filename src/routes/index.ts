import { Router } from 'express'
import { PageNotFoundError } from '../errors'
import { cartRoutes } from './cart.routes'
import { productsRoutes } from './products.routes'
import { usersRoutes } from './users.routes'

export * from './products.routes'
export * from './users.routes'

export const appRoutes = Router()

appRoutes.use('/users', usersRoutes)
appRoutes.use('/products', productsRoutes)
appRoutes.use('/cart', cartRoutes)
// appRoutes.use('/payments', paymentsRoutes)
// appRoutes.use('/coupons', couponsRoutes)
// appRoutes.use(usersRoutes)


appRoutes.get('*', (req, res, next) => {
  // res.status(404).json({ message: 'Page Not Found' })
  next(new PageNotFoundError())
})