// const http = require('http')
import * as http from 'http'

const hostname = process.env.HOST || '127.0.0.1'
const port = Number(process.env.PORT || '8080')

var counter = 0; // shared 
const server = http.createServer((req, res) => {
  // debugger
  // var counter = 0; // not shared // stateless
  res.statusCode = 200
  res.setHeader('Content-Type', 'text/html')

  if (req.url?.includes('counter')) {
    res.end(`<h1>${counter++}</h1>`)
  }
  else if (req.url?.includes('calculate')) {
    const now = Date.now()
    // while (now + 15_000 > Date.now()) {/* Simulate CPU intensive work */ }
    const handle = setInterval(() => {
      if ((now + 15_000) < Date.now()) {
        res.end(`<h1>Calculation done!</h1>`)
        clearInterval(handle)
      }
    }, 100)

  }
  else if (req.url?.includes('bigfile')) {
    // let bigfile = '';
    // for (let i = 0; i < 10e5; i++) {
    //   bigfile += ' Ala ma kota \r\n <br>';
    // }
    // res.end(bigfile)
    let i = 0
    const b = Buffer.from('Ala ma kota        \r\n  <br/>')
    const handle = setInterval(() => {
      b.write(i.toString(), 12, 'utf-8')
      res.write(b)
      if (i++ > 10e5) {
        res.end()
        clearTimeout(handle)
      }
    }, 10)
  }
  else {
    res.end('Hello Node Typescript World!\n')
  }
})

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`)
})
// node index.js
// node .

// https://nodejs.org/api/process.html#process_signal_events
process.on('SIGINT', () => {
  console.log('Closing ...')
  server.close(() => {
    process.exit(0)
  })
})