import { Application, ErrorRequestHandler } from "express";
import { PageNotFoundError } from ".";
import { AccessDenied } from "./AccessDenied";


export const errorHandlers = (app: Application) => {

  const errorHandler: ErrorRequestHandler = (error, req, res, next) => {

    if (error instanceof PageNotFoundError) {
      res.status(error.status).send({
        message: error.message
      })
      return;
    }

    if (error instanceof AccessDenied) {
      res.status(error.status).send({
        message: error.message
      })
      return;
    }

    res.status(500).json({
      message: error.message
    })
  }
  app.use(errorHandler)


  process.on('unhandledRejection', (error) => {
    console.error('unhandledRejection', error)
  })
}