
export class PageNotFoundError extends Error {
  message = 'Page Not Found'
  status = 404
}