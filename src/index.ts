
import express from 'express'
import { appRoutes } from './routes'
import { configMiddlewares } from './config/middlewares'
import { errorHandlers } from './errors/errorHandlers'
import mongoose from 'mongoose'

const app = express()

configMiddlewares(app)

app.use(appRoutes)

app.use(errorHandlers)

const hostname = process.env.HOST || '127.0.0.1'
const port = Number(process.env.PORT || '8080')


mongoose.connect("mongodb://localhost:27017/database", {
  useNewUrlParser: true,
  useUnifiedTopology: true 
  /* autoIndex: process.env.NODE_ENV === "development" */
}).then(() => console.log('Mongo connected!'))
  .catch(console.error)


const server = app.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`)
})

process.on('SIGINT', () => {
  console.log('Closing ...')
  server.close(() => {
    process.exit(0)
  })
})
