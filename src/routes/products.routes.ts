
import { Router } from 'express'
import path from 'path'
import fs from 'fs'

export const productsRoutes = Router()

const productsDataPath = path.join(__dirname, '..','..', 'data', 'products.json');
const productsData: SimpleProduct[] = JSON.parse(fs.readFileSync(productsDataPath).toString('utf-8'))


// /products?filter=dress
productsRoutes.get('/', (req, res) => { 
  const { filter = null } = req.query;

  let data = filter ? productsData.find(p => {
    return p.name.toLocaleLowerCase().includes(filter?.toLocaleString())
  }) : productsData

  res.json(data)
})

// /products/1270
productsRoutes.get('/:product_id', (req, res) => {
  const { product_id } = req.params;

  const productFound = productsData.find(p => p.id == product_id);

  res.json(productFound)
})

// const p = {
//   "id": "1270",
//   "name": "Mens Pink Summer Hoodie Face Print",
//   "price": 88.99
// }

interface SimpleProduct {
  id: string;
  name: string;
  price: number;
}