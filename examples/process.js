// node --inspect -- samsung-nodejs/examples/process.js ala --ma kota
console.log(process.execArgv)
console.log(process.execPath)

//  node samsung-nodejs/examples/process.js ala --ma kota
console.log(process.argv0)
console.log(process.argv)

// https://www.npmjs.com/package/commander

// PORT=666 node --inspect -- samsung-nodejs/examples/process.js ala --ma kota
console.log(process.env.PORT)

console.log(process.pid)
console.log(process.cwd())
console.log(__dirname)
console.log(__filename)

process.chdir(__dirname)
console.log(process.cwd())