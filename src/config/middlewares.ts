import express, { Application, RequestHandler } from "express"
import path from "path"

import requestId from 'express-request-id'
import session from 'express-session'
import timeout from 'connect-timeout'
import morgan from 'morgan'
import multer from 'multer'
import cors from 'cors'
import helmet from 'helmet'


export const configMiddlewares = (app: Application, options = {}) => {

  app.use(cors({
    // origin: '*'
    // credentials:true // cookies cross-domain
  }))
  app.use(morgan(process.env.NODE_ENV === 'production' ? 'combined' : 'dev'))
  process.env.NODE_ENV === 'production' && app.use(timeout('5s'))
  // app.use(multer()
  app.use(helmet({
    contentSecurityPolicy: {
      reportOnly: true
    }
  }))
  app.use(requestId())

  app.use(express.urlencoded()) // parse forms
  app.use(express.json({})) // parse json
  app.use('/images', express.static(path.join(__dirname, '..', '..', 'data/images'), {
    extensions: ['jpg', 'jpeg']
  }))
  app.use('/public', express.static(path.join(__dirname, '..', '..', 'public'), {
    // fallthrough:true // next('route')
    // immutable: true // for hashed files cache forever
  }))

  /* ==== SESSION ==== */
  // app.use(session())
  app.set("trust proxy", 1); // trust first proxy
  // cors({ // credentials:true // cookies cross-domain
  app.use(session({
    secret: process.env.SESSION_SECRET || "keyboard cat",
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false, maxAge: 60000 }
  }));


  app.use(passport.initialize());
  app.use(passport.session());

}
import passport from 'passport'
import { Strategy as LocalStrategy } from 'passport-local'
import { getUserById, logInUser } from "../services/users.service"

passport.use(new LocalStrategy({
  usernameField: 'username',
  passwordField: 'password'
}, async function (username, password, done) {
  try {
    const user = await logInUser(username, password)
    if (!user) {
      return done(null, null);
    }
    return done(null, user);
  } catch (err) {
    return done(err)
  }
}));

passport.serializeUser(function (user: { _id: string }, done) {
  done(null, user._id.toString());
});

passport.deserializeUser(async function (id: string, done) {
  try {
    const user = await getUserById(id)
    const userData = user.toObject()
    done(null, userData);

  } catch (err) {
    done(err, null);
  }
})


declare global {
  namespace Express {
    // Inject additional properties on express.Request
    interface Request {
      isAdmin?: boolean
    }
  }
}


export const onlyLoggedIn = (roles: string[] = []): RequestHandler => (req, res, next) => {
  next()
}

export const onlyAdmin: RequestHandler = (req, res, next) => {
  if (req.headers['authorization']) {
    req.isAdmin = true
    next()
  } else {
    next(new Error('Not Authorized'))
  }
}