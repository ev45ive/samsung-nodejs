# Git 
git clone https://bitbucket.org/ev45ive/samsung-nodejs.git samsung-nodejs
cd samsung-nodejs
npm install
npm start

# Init new project
npm init -y 

# Typescript
npm.cmd i -g typescript 

https://www.typescriptlang.org/tsconfig/
npm i -g typescript 
tsc --init 
<!-- message TS6071: Successfully created a tsconfig.json file. -->

npm i -D typescript @types/node

npm i -D nodemon
nodemon dist/index.js

# Deployment
npm install --production -ci

# Middleware
npm i express-request-id express-session connect-timeout morgan  multer cors helmet

npm i --save-dev @types/express-request-id @types/express-session  @types/connect-timeout  @types/morgan   @types/multer  @types/cors

http://expressjs.com/en/resources/middleware/timeout.html
<!-- app.use(timeout('5s')) -->

http://expressjs.com/en/resources/middleware/morgan.html
<!-- morgan(':method :url :status :res[content-length] - :response-time ms') -->

# Security
https://github.com/helmetjs/helmet

// This...
app.use(helmet());

// ...is equivalent to this:
app.use(helmet.contentSecurityPolicy());
app.use(helmet.dnsPrefetchControl());
app.use(helmet.expectCt());
app.use(helmet.frameguard());
app.use(helmet.hidePoweredBy());
app.use(helmet.hsts());
app.use(helmet.ieNoOpen());
app.use(helmet.noSniff());
app.use(helmet.permittedCrossDomainPolicies());
app.use(helmet.referrerPolicy());
app.use(helmet.xssFilter());


# Session 
http://expressjs.com/en/resources/middleware/session.html
https://github.com/jaredhanson/passport

# Authorization
npm install passport passport-local
npm install -D @types/passport @types/passport-local

http://www.passportjs.org/packages/passport-local/
http://www.passportjs.org/packages/passport-local-token/
http://www.passportjs.org/packages/passport-http-oauth/
http://www.passportjs.org/packages/passport-jwt/


# Mongo
docker run -dit -p 27017:27017 --name=mongo mongo
docker exec -dit mongo mongo
show dbs
use database
db.find({})


# File Upload
https://github.com/mscdex/busboy#busboy-methods
http://expressjs.com/en/resources/middleware/multer.html

# 12 factor
https://12factor.net/pl/


# Monitoring
https://github.com/siimon/prom-client
https://pm2.keymetrics.io/docs/usage/monitoring/
https://prometheus.io/docs/concepts/metric_types/

# Microservices - tracing
https://opentracing.io/guides/javascript/

# Pm2 
https://pm2.keymetrics.io/docs/usage/signals-clean-restart/
ecosystem.config.js
pm2 start
pm2 scale app 3
pm2 log
pm2 reload app
